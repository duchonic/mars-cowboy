pico-8 cartridge // http://www.pico-8.com
version 29
__lua__
--mars cowboy
--space game for jam

debug=true
playmusic=true
time=0
darkmode=false
level=0
destruction_timer_define=40
globalspacesize=2000
rare_earth={false,false,false,false}

function _init()
	mainmenu = true
	intro = true
	player_init()
	menu_init()
	if(darkmode)then
		poke(0x5f2e,1)
	end
	if(darkmode)then
		for i=1,15 do
			pal(i,i+128,1)
		end
	end
	stars={}
	mars={}
	craters={}
	if level == 0 then
		add(mars,{x=64,y=128+100,r=128})
	elseif level == 2 then
		add(mars,{x=64,y=-120,r=128})
	else
		add(mars,{x=64,y=6400,r=128})
	end
	for c=0,200 do
		add(craters,{x=rnd(256)-128,y=rnd(256)-128,r=rnd(5),c=0})
	end
	for s=1,10 do
		add(stars,{x=rnd(128),y=rnd(128),r=rnd(2)})
	end
end

function _update()

	time+=1
	if(mainmenu)then
		menu_update()
	else
		--check if all rare earth are found
		allfound=true
		for r in all(rare_earth)do
			if(r==false)then
				allfound=false
				break
			end
		end
		space_update()
		enemy_update()
		player_update()
	end
	myEffects:update()
end

function _draw()
	cls()
	if debug then 
		print("MARS COWBOY VO.5", 2, 2, 2)
	end
	if(mainmenu)then
		menu_draw()
		myEffects:draw()
	else
		space_draw_back()
		enemy_draw()
		stats_draw()
		myEffects:draw()
		player_draw(true) 
		space_draw_front()
	end
	debug_draw()
end

-->8
-- defines for game
function defines()end

DEF={
	ARM_LEFT=0,
	ARM_RIGHT=1,
	ARM_DOWN=2,
	ARM_MID=3,
	ARM_UP=4,
	FUEL = 10,
	HOMEBASE = 11,
	OXYGEN = 12,
	SOUND={	
		PLAYER={
			leftarm = 1,
			rightarm = 1,
			ground_release = 2,
			ground_touch = 3,
			collision_head = 4,
			collision_body = 5,
		},
		
		enemy_explode=6,
		enemy_spray=7,

		firearms_shot=10,
		laser1_shot=11,
		laser2_shot=12,
		ui_select=13,
		ui_confirm=14,
		short_whoosh=15,
		long_whoosh=16,
		ui_denying=17,
		robot_noize=18,
	},
	TEXT={
		LEVEL={"normal","space","hard"},
		CHARACTER={"0Xrna","0Xm8","0Xcpp"},	
	},
}

-->8
-- menuitem add some MENUS
local pbap=false
menuitem(1,"palette",
	function()
		pbap = not pbap
		for i=1,15 do
			pal(i,i+(pbap and 128 or 0),1)
		end
	end)
menuitem(2,"map",
	function()
		rect(10,10,120,120,7)
	end)
menuitem(3,"debug",
	function()
		debug = not debug
	end)
menuitem(4,"map size 3000",
	function()
		globalspacesize = 3000
	end)
menuitem(5,"map size 5000",
	function()
		globalspacesize = 5000
	end)

function menu_init()
	--square={{1,1},{-1,1},{-1,-1},{1,-1}}
end


function menu_update()
	if(intro)then
		if(btnp(❎))then
			intro=false
		end
		return
	end

	if(btnp(➡️))then
		if(level<2)then 
		  sfx(DEF.SOUND.ui_select)
			level+=1
			player.left+=1
			player.right+=1
		else
			sfx(DEF.SOUND.ui_denying)
		end 
	end
	if(btnp(⬅️))then
		if(level>0)then
			sfx(DEF.SOUND.ui_select)
			level-=1
			player.left-=1
			player.right-=1
		else
			sfx(DEF.SOUND.ui_denying)
		end 
	end
	if(btnp(⬆️))then
		sfx(DEF.SOUND.short_whoosh)
		player.character+=1
		player.character%=3
	end
	if(btnp(⬇️))then
		sfx(DEF.SOUND.short_whoosh)
			player.character-=1
			player.character%=3
	end
	if(btnp(❎))then
		sfx(DEF.SOUND.ui_confirm)
		game_init()
		mainmenu=false
	end
end

function menu_draw()
	if(intro)then
		text = {" Steel (metal) blood courses",
		 		"through your veins... ",
				"Hunt down your former",
				"friends to make a buck",
				"just to survive.", 
				" You forgot your name (code)",
				"but you remember the taste",
				"of victory (oil).",
				" Signs of death echo",
				"across the dusty,",
				"lifeless red earth!!",
				" It's lubricant (blood) for",
				"coolant (blood) as this ",
				"32 player battle erupts!",
				"",
				"MARS COWBOY!!!!!"}
		local y=16
		for line=1,16  do
			print(text[line],8,y,rnd(3)+1)
			y+=6
		end

		return
	end

	if level == 0 then
		mars[1].x=64
		mars[1].y=128+100
		mars[1].r=128
	elseif level == 2 then
		mars[1].x=64
		mars[1].y=-110
		mars[1].r=128
	else
		mars[1].x=64
		mars[1].y=6400
		mars[1].r=1
	end



	draw_mars()

	p={}
	if level==0 then
		add(p,{n=132,x=56,y=2})
		add(p,{n=133,x=120,y=64})
		add(p,{n=134,x=56,y=120})
		add(p,{n=135,x=2,y=64})
		map(0, 0, 0, 0, 16, 16)
		--map(32,2,mars[1].x,mars[1].y,14,2)
	elseif level==1 then
		add(p,{n=132,x=8,y=2})
		add(p,{n=133,x=112,y=2})
		add(p,{n=134,x=112,y=120})
		add(p,{n=135,x=8,y=120})
		map(16, 0, 0, 0, 16, 16)
	else
		add(p,{n=132,x=rnd(120),y=rnd(120)})
		add(p,{n=133,x=rnd(120),y=rnd(120)})
		add(p,{n=134,x=rnd(120),y=rnd(120)})
		add(p,{n=135,x=rnd(120),y=rnd(120)})
		--map(32,0,mars[1].x,mars[1].y,14,2)
	end
	for i in all(p) do
		spr(i.n,i.x,i.y)	
	end

	player_draw(false)
	print("SELECT PLAYER ⬆️⬇️",28,24,7)
	print("  AND LEVEL ⬅️➡️", 28,30,7)
	print(" PRESS X TO START", 28, 36)

	print(DEF.TEXT.CHARACTER[player.character+1],56,80)
	print("level: " .. DEF.TEXT.LEVEL[level+1],40,88)
end

-->8
-- draw 3d objects

function draw_stars()
	for s in all(stars) do
		circ(s.x,s.y,s.r,7)
	end
end

function draw_mars()
	local x = mars[1].x
	local y = mars[1].y
	
	circfill(mars[1].x,mars[1].y,mars[1].r-4,8)
	for c in all(craters)do
		circfill(x+c.x,y+c.y,c.r,c.c)	
	end
	
end

function draw_planet_ring(o,t,s,rx,ry)
	if(t/s%1 < 0.2 or t/s%1 > 0.3)then
  	pset(o.x+4+cos(t/s)*rx,o.y+4+sin(t/s)*ry,o.c+flr(rnd(2)-1))
	end	
end


function draw_circle(o,s,rx,ry,c)
	x1=o.x+rx/2*cos(time/s)
	y1=o.y
	x2=o.x+rx-cos(time/s)*rx/2
	y2=o.y+ry*2
	ovalfill(x1,y1,x2,y2,c)
	oval(x1,y1,x2,y2,5)
end

function draw_poly(x,y,c,s,angle)
	osx=x
	osy=y
	size=s
	square={
		{cos(angle),sin(angle)},
		{cos(angle+0.25),sin(angle+0.25)},
		{cos(angle+0.5),sin(angle+0.5)},
		{cos(angle+0.75),sin(angle+0.75)}
	}

	sp=square[4]
	pointsa={}
	pointsb={}
	for p in all(square)do
		line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
		add(pointsa,{osx+p[1]*size,osy+p[2]*size})
		sp=p
  end
	osx+=size/2
	osy+=size/2
	for p in all(square)do
		line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
		add(pointsb,{osx+p[1]*size,osy+p[2]*size})
		sp=p
  end
	for n=1,4 do
		line(pointsa[n][1],pointsa[n][2],pointsb[n][1],pointsb[n][2],c)
	end
end


function draw_cube(x,y,c,s)
	osx=x
	osy=y
	size=s

	square={{cos(time/100),sin(time/100)},
		{cos(time/100+0.25),sin(time/100+0.25)},
		{cos(time/100+0.5),sin(time/100+0.5)},
		{cos(time/100+0.75),sin(time/100+0.75)}}

	sp=square[4]
  pointsa={}
	pointsb={}
	for p in all(square)do
		line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
		add(pointsa,{osx+p[1]*size,osy+p[2]*size})
		sp=p
  end
	osx+=size/2
	osy+=size/2
	for p in all(square)do
		line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
		add(pointsb,{osx+p[1]*size,osy+p[2]*size})
		sp=p
  end
	for n=1,4 do
		line(pointsa[n][1],pointsa[n][2],pointsb[n][1],pointsb[n][2],c)
	end
end


objects={

	polygon={},

	Make = function(self)
		p={}
		edges=5
		s=100
		for i=1,edges do
			add(p, {cos(time/s+i/edges), sin(time/s+i/edges)})
		end
		add(self.polygon, p)
	end,
	Update = function(self)

	end,
	Draw = function(self)
		size=12
		c=7
		osx=80
		osy=30
		for o in all(self.polygon) do
			debug3 = #o
			sp=o[#o]
			for p in all(o)do
				line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
				sp=p
			end

		end
	end
}


function draw_debri(x,y,s,c)
	osx=x
	osy=y
	size=8
	polygon={}
	edges=3
	for i=1,edges do
		add(polygon, {cos(time/s+i/edges), 0.3*sin(time/s+i/edges)})
	end
	debug4 = #polygon
	sp=polygon[#polygon]

	for p in all(polygon)do
		line(osx+sp[1]*size,osy+sp[2]*size,osx+p[1]*size,osy+p[2]*size,c)
		sp=p
  	end
end


-->8
-- stats
function stats_draw()
	
	if(rare_earth[1])then
		--spr(180,16,112)
		draw_cube(24,112,1,5)
	end
	if(rare_earth[2])then
		--spr(181,8,104)
		draw_cube(16,104,2,5)
	end
	if(rare_earth[3])then
		--spr(182,0,112)
		draw_cube(8,112,3,5)
	end
	if(rare_earth[4])then
		--spr(183,8,120)
		draw_cube(16,120,4,5)
	end
	
	if(allfound)then
		print("RETURN HOME",26,122,7)
	else	
		print("FIND FOUR RARE EARTH",26,122,7) 
	end
end
-->8 -- game 

function game_init()
	space_init()
	enemy_init()
	time=0
	if(playmusic)then
		music(10)
	end
end

function game_restart()
	myEffects:AddExplode(player,100)
	sfx(DEF.SOUND.enemy_explode)
	rare_earth={false,false,false,false}
	mainmenu = true
	player_restart()
end

-->8
-- space

function space_init()

	--the space
	space={planet={},belt={},spacemap={},size=globalspacesize}
	for i=0,10 do
		size = space.size
		add(space.belt, {w=2,h=2,nr=68+flr(rnd(4))*2,x=space_rnd_point(),y=space_rnd_point()})
	end
	for i=0,10 do
		add(space.planet, {w=4,h=4,nr=64,x=space_rnd_point(),y=space_rnd_point(),dx=rnd(0.02)-0.01,dy=rnd(0.02)-0.01})
	end
	for i=0,25 do
		add(space.spacemap, {nr=100+rnd(4),x=space_rnd_point(),y=space_rnd_point()})
	end

	--platforms
	platforms={}
	
	homebase={ {64,72}} --{72,72},{80,72} }
	
	
	for i=1,#homebase do
		add(platforms, {nr=128,touched=false,x=homebase[i][1],y=homebase[i][2],w=8,h=8,
			c=DEF.HOMEBASE})
	end
	
	--to the right
	for i=1,2 do
		add(platforms, {nr=130,touched=false,x=64+i*500/(2*3),y=96,w=8,h=8,
			c=DEF.FUEL})
		add(platforms, {nr=130,touched=false,x=64-i*500/(2*3),y=96,w=8,h=8,
			c=DEF.FUEL})
	end

	--oxygen
	--to the right
	for i=1,2 do
		add(platforms, {nr=129,touched=false,x=64+i*500/(2*3),y=0,w=8,h=8,
			c=DEF.OXYGEN})
	end
	--to the left
	for i=1,2 do
		add(platforms, {nr=129,touched=false,x=64-i*500/(2*3),y=0,w=8,h=8,
			c=DEF.OXYGEN})
	end

	--rare earth
	local dis=space.size/2

	--to the right
	if level==0 then
		pos={ {x=0,y=-1},{x=1,y=0},{x=0,y=1},{x=-1,y=0}}
	elseif level==1 then
		pos={ {x=-1,y=-1},{x=1,y=-1},{x=1,y=1},{x=-1,y=1} }
	else
		pos={ {x=-rnd(1),y=-rnd(1)},{x=rnd(1),y=-rnd(1)},{x=rnd(1),y=rnd(1)},{x=-rnd(1),y=rnd(1)} }
	end

	add(platforms, {nr=132,touched=false,x=64+pos[1].x*dis,y=64+pos[1].y*dis,w=5,h=8,c=1})
	--on the top
	add(platforms, {nr=133,touched=false,x=64+pos[2].x*dis,y=64+pos[2].y*dis,w=5,h=8,c=2})
	--on the left
	add(platforms, {nr=134,touched=false,x=64+pos[3].x*dis,y=64+pos[3].y*dis,w=5,h=8,c=3})
	--on the bottom
	add(platforms, {nr=135,touched=false,x=64+pos[4].x*dis,y=64+pos[4].y*dis,w=5,h=8,c=4})
end

function space_update()

end

function space_draw_platform(o)

	if( (o.c==1 or o.c==2 or o.c==3 or o.c==4) and not o.touched )then
		draw_cube(o.x+2,o.y-10,o.c,o.w)
	end

	if(o.c==1 and not o.touched)print("RARE EARTH",o.x+10,o.y,o.c)
	if(o.c==DEF.OXYGEN and not o.touched)print("OXYGEN",o.x+10,o.y,o.c)
	if(o.c==DEF.FUEL and not o.touched)print("FUEL",o.x+10,o.y,o.c)
	if(o.c==DEF.HOMEBASE)print("HOME",o.x+10,o.y,o.c)

	local sprnr=0
	if(o.c==DEF.FUEL)sprnr=130
	if(o.c==DEF.HOMEBASE)sprnr=128
	if(o.c==DEF.OXYGEN)sprnr=129
	if(o.c==1)sprnr=132
	if(o.c==2)sprnr=133
	if(o.c==3)sprnr=134
	if(o.c==4)sprnr=135

	spr(sprnr+(flr(time/10)%4*16) ,o.x,o.y)

	for i=1,20 do
		draw_planet_ring(o,time+i,100,8,2)
	end
	for i=15,20 do
		draw_planet_ring(o,time+i,120,9,3)
	end
	for i=30,35 do
		draw_planet_ring(o,time+i,210,10,4)
	end

end

function space_draw_front()
	--foreach(platforms, space_draw_platform())
	for p in all(platforms)do
		space_draw_platform(p)
	end
	for p in all(space.planet)do
		p.x+=p.dx
		p.y+=p.dy
		spr(p.nr,p.x,p.y,p.w,p.h)
	end
end

function space_draw_back()
	
	if level == 0 then
		--map(32,2,mars[1].x,mars[1].y,14,2)
	elseif level == 2 then
		--map(32,0,mars[1].x,mars[1].y,14,2)
	end
	
	draw_stars()
	draw_mars()
 
	for o in all(space.spacemap) do
		distance = abs(player.x-o.x)+abs(player.y-o.y)
		if(distance<256)then
			spr(o.nr,o.x,o.y) 
		end
	end
	for p in all(space.belt) do
		distance = abs(player.x-p.x)+abs(player.y-p.y)
		if(distance<256)then
			spr(p.nr,p.x,p.y,p.w,p.h)
		end
	end
end

function space_rnd_point()
	return rnd(space.size)-(space.size/2)
end

function space_move_X()
	for b in all(platforms)do
		b.x-=player.dx
	end
	for p in all(space.planet) do
		p.x-=player.dx*1.5	
	end
	for o in all(space.belt) do
		o.x-=player.dx/3
	end
	for o in all(space.spacemap) do
		o.x-=player.dx/4
	end
	mars[1].x-=player.dx/32
end

function space_move_Y()
	for b in all(platforms)do
		b.y-=player.dy
	end
	for p in all(space.planet) do
		p.y-=player.dy*1.5
	end
	for p in all(space.belt) do
		p.y-=player.dy/3
	end
	for o in all(space.spacemap) do
		o.y-=player.dy/4
	end
	mars[1].y-=player.dy/32
end





-->8
-->enemy
function enemy_init()
	enemy={}
	for i=1,50 do
		local color=10+flr(rnd(2))*2
		add(enemy, {
				dx=rnd(0.4)-0.2,dy=rnd(0.3)-0.15, spr=10+flr(rnd(3))*2,spray=30+flr(rnd(30)),
				x=space_rnd_point(),y=space_rnd_point(),w=5+flr(rnd(10)),h=16,
				f={dx=1,dy=1,die=10,r=3,c=color,c_table={2,color,3,color},amount=12},
				angle=rnd(1),
				rotation=(rnd(2)-1)/100,
				Draw = function(self)
					draw_poly(self.x,self.y,self.f.c,self.w,self.angle)
				end,
				Spray = function(self)
					myEffects:AddTrail(self)
					sfx(DEF.SOUND.enemy_spray)
				end,
				Move = function(self)
					self.x+=self.dx
					self.y+=self.dy
				end,
				Kill = function(self)

					myEffects:AddExplode(self,10)
					sfx(DEF.SOUND.enemy_explode)
					del(enemy,self)
				end,
			})
	end
end


function enemy_update()
	for e in all(enemy)do
		distance = abs(player.x-e.x)+abs(player.y-e.y)
		if(distance < 300)then
			if(collision(player,e))then
				e:Kill()		
				if(player.character!=1)then
					if(not debug)then
						game_restart()
					end
				end
			end
		
			e.angle+=e.rotation
			e:Move()
			if e.w <= 4 then
				add(platforms, {nr=130,touched=true,x=e.x,y=e.y,w=8,h=8,c=e.f.c})
				e:Kill()
			end
		end
	end
end


function enemy_draw()
 for e in all(enemy)do
  	distance = abs(player.x-e.x)+abs(player.y-e.y)
	if(distance < 228)then
	 if( (time%e.spray)==0 )then
		e:Spray()
   end
	 e:Draw()
	end
 end
end

function enemy_move_Y()
	for b in all(enemy)do
		b.y-=player.dy
	end
end

function enemy_move_X()
	for b in all(enemy)do
		b.x-=player.dx
	end
end

-->8
-- player
function player_init()
	player={character=0,fuel=90,oxygen=80,absx=0,absy=0,x=60,y=48,
			dx=0,dy=0,w=8,h=24,head=0,left=level,
			right=level,ground=false,destruction=destruction_timer_define,
			f={dx=2,dy=2,die=10,r=3,c=11,c_table={0,9,10,11},amount=1},
			direction={left="none",right="none"},
		Spray = function(self)
			if (player.fuel > 0) myEffects:AddFire(self)
		end,
		Gravity = function(self)
			if(level==0)then
				if(self.dy<9)then self.dy+=.1 end
			elseif(level==1)then
			elseif(level==2)then
				if(self.dy>-9)then self.dy-=.1 end
			end
		end,

		Accel = function(self)
			local trust = .1
			local fuellost = 0.1
			if (player.fuel <= 0) then return end

			if self.direction.left == "down" then
				self.dy -= trust
				self.dx += trust
			elseif self.direction.left == "up" then
				self.dy += trust
				self.dx += trust 
			elseif self.direction.left == "middle" then
				self.dx += 2*trust
			end

			if self.direction.left != "none" then 
				if not debug then player.fuel-=fuellost end
				sfx(DEF.SOUND.PLAYER.leftarm)
			end
			if self.direction.right != "none" then 
				if not debug then player.fuel-=fuellost end
				sfx(DEF.SOUND.PLAYER.rightarm)
			end

			if self.direction.right == "down" then
				self.dy -= trust 
				self.dx -= trust 
			elseif self.direction.right == "up" then
				self.dy += trust 
				self.dx -= trust 
			elseif self.direction.right == "middle" then
				self.dx -= trust 
			end

			if(self.ground and (self.direction.left == "down") and (self.direction.right == "down"))then
				if (self.y > 16)then
					self.y-=2
					self.absy-=2
				end
			end
			
			local maxspeed = 2
			if(self.dy>maxspeed)self.dy=maxspeed
			if(self.dy<-maxspeed)self.dy=-maxspeed
			if(self.dx>maxspeed)self.dx=maxspeed
			if(self.dx<-maxspeed)self.dx=-maxspeed
		end,
	}
end


function player_update()
	player.direction.left="none"
	player.direction.right="none"
	debug3 = flr(player.absx) .. '/' .. flr(player.absy)
	if(time%10 == 0 and not debug)player.oxygen-=0.2
	if(player.oxygen<=0)game_restart() 

	if(level==0)then
		if(btn(⬆️))then 	
			player.left=level
			player.right=level
			player.direction.left="down"
			player.direction.right="down"
		elseif(btn(⬇️))then
			player.destruction-=1
			if(player.destruction==0)then
				game_restart() 
			end
		else
			player.destruction=destruction_timer_define
		end
	elseif(level==2)then
		--position
		if(btn(⬇️))then
			player.left=level
			player.right=level
			player.direction.left="up"
			player.direction.right="up"
		elseif(btn(⬆️))then 	
			player.destruction-=1
			if(player.destruction==0)then
				game_restart() 
			end
		else
			player.destruction=destruction_timer_define
		end
	elseif(level==1)then
		if(btn(⬆️))then 	
			player.left=0
			player.right=0
			player.direction.left="down"
			player.direction.right="down"
		elseif(btn(⬇️))then
			player.destruction-=1
			if(player.destruction==0)then
				game_restart() 
			end
		else
			player.destruction=destruction_timer_define
		end
	end

	if(btnp(⬅️)) then
		player.left+=1 
		player.left%=3
		debug4=flr(rnd(2))*2
	end
	if(btnp(➡️)) then
		player.right+=1 
		player.right%=3
	end

	-- left arm
	if(btn(5))then
		if(player.fuel>0)then
			if(player.left==0)then
				player.direction.left="down"
			elseif(player.left==1)then
				player.direction.left="middle"
			else
				player.direction.left="up"
			end
		end
	end
	-- right arm
	if(btn(4))then
		if(player.fuel>0)then
			if(player.right==0)then
				player.direction.right="down"
			elseif(player.right==1)then
				player.direction.right="middle"
			else
				player.direction.right="up"
			end
		end
	end

	player:Spray()
	player:Accel()
	player:Gravity()

	player_check_collision()

	player.absx += player.dx
	player.absy += player.dy
	player.x += player.dx
	player.y += player.dy

	if(player.x < 32)then
		player.x -= player.dx
		space_move_X()
		enemy_move_X()
	end
	if(player.x > 96)then
		player.x -= player.dx
		space_move_X()
		enemy_move_X()
	end
	if(player.y < 16)then
		player.y -= player.dy
		space_move_Y()
		enemy_move_Y()
	end
	if(player.y > 64)then
		player.y -= player.dy
		space_move_Y()
		enemy_move_Y()
	end

	--head
	player.head+=1

end

function player_draw(compass)
	
	if(compass)player_compass_draw()
	if(player.destruction!=destruction_timer_define)print(player.destruction, player.x+10,player.y,7)

	if(player.character==0)then
		body={39,32}
	elseif(player.character==1)then
		fillp(flr(rnd(200))) -- or 0x33cc.8
		ovalfill(player.x-4,player.y-4,player.x+12,player.y+28,1)
		fillp(0x0000) -- or 0x33cc.8
		body={40,33}
	elseif(player.character==2)then
		body={41,34}
	end

	if(player.left==2)spr(4,player.x-8,player.y)
	if(player.right==2)spr(3,player.x+8,player.y)
	spr(19+player.left%3,player.x-8,player.y+8)
	spr(35+player.right%3,player.x+8,player.y+8)

	if(player.dx < 0)then
		--head
		spr(16+player.head%3,player.x,player.y)
		--body
		spr(body[1],player.x,player.y+8)
		--feet
		if(player.ground)then
			spr(49,player.x,player.y+16)
		else
			spr(50,player.x,player.y+16)
		end
	else
		--head
		spr(23+player.head%3,player.x,player.y)
		--body
		spr(body[2],player.x,player.y+8)
		--feet
		if(player.ground)then
			spr(51,player.x,player.y+16)
		else
			spr(52,player.x,player.y+16)
		end
	end
end

function player_restart()
	player.x=60
	player.y=48 
	player.dx=0
	player.dy=0
	player.absx=0
	player.absy=0
	player.oxygen=80
	player.fuel=90
	player.destruction=destruction_timer_define 
end 


function player_check_collision()

	feetCollision = false
	headCollision = false

	for o in all(platforms) do
		if(collision(o,{x=player.x+2,y=player.y+22,h=2,w=4}))then
			feetCollision=true
			o.touched = true

			if(o.c==1)then
				if(not rare_earth[1])then
					sfx(DEF.SOUND.robot_noize)
					rare_earth[1]=true
				end
			elseif(o.c==2)then
				if(not rare_earth[2])then
					sfx(DEF.SOUND.robot_noize)
					rare_earth[2]=true
				end
			elseif(o.c==3)then
				if(not rare_earth[3])then
					sfx(DEF.SOUND.robot_noize)
					rare_earth[3]=true
				end
			elseif(o.c==4)then
				if(not rare_earth[4])then
					sfx(DEF.SOUND.robot_noize)
					rare_earth[4]=true
				end
			end
			if(o.c==11)then
				if(allfound)then
					game_restart()
				end
			end
			if(o.c==12 or o.c==11)then
				if(player.oxygen<100)then
					player.oxygen+=10
				end
			end
			if(o.c==10 or o.c==11)then
				if(player.fuel<100)then
					player.fuel+=10
				end
			end
		end
		if(collision(o,{x=player.x,y=player.y,h=8,w=8}))then
			headCollision=true
		end
	end

	if(headCollision)then
		player.dy = 2
		myEffects:AddExplode(player,10)
		sfx(DEF.SOUND.PLAYER.collision_head)
		if(not debug)then
			game_restart()
		end
	elseif(not feetCollision)then
		for o in all(platforms) do
			--check body collision
			if(collision(o,player))then
				myEffects:AddExplode(player,2)
				player.dx*=-1
				sfx(DEF.SOUND.PLAYER.collision_body)
				break 
			end
		end

		if(player.ground)then
			sfx(DEF.SOUND.PLAYER.ground_release)
		end

		player.ground = false
	else
		player.dy = 0
		if(player.ground==false)then
			sfx(DEF.SOUND.PLAYER.ground_touch)
		end
		player.ground = true
		player.dx = 0
	end
end

function collision(a,b)
	return (b.x < a.x + a.w and b.x + b.w > a.x and b.y < a.y + a.h and b.y + b.h > a.y)
end

function player_compass_power(p)
	px=player.x+15
	py=player.y-5
	dx=p.x-px
	dy=p.y-py
	dist = abs(dx)+abs(dy)  
	if(dist<200 and p.touched)then
		tx=px+dx/3+4
		ty=py+dy/3+4
		line(px,py,tx,ty,p.c)
		circ(tx,ty,1,p.c)
	end
end

function player_compass_oxygen(p)
	px=player.x+10
	py=player.y-10
	dx=p.x-px
	dy=p.y-py
	dist = abs(dx)+abs(dy)  
	if(dist<200 and p.touched)then
		tx=px+dx/3+4
		ty=py+dy/3+4
		line(px,py,tx,ty,p.c)
		circ(tx,ty,1,p.c)
	end
end

function player_compass_homebase(p)
	px=player.x+4
	py=player.y+12
	dx=p.x-px
	dy=p.y-py
	dist = abs(dx)+abs(dy)  
	if(p.touched)then
		tx=px+dx/3+4
		ty=py+dy/3+4
		line(px,py,tx,ty,p.c)
		circ(tx,ty,1,p.c)
	end
end

function player_compass_draw()
	for b in all(platforms)do
		if(b.c==10)then
			player_compass_power(b)
		elseif(b.c==11)then
			player_compass_homebase(b)
		elseif(b.c==12)then
			player_compass_oxygen(b)
		end
	end

	print("OXYG",player.x+20,player.y-15,12)
	print("FUEL",player.x+24,player.y-8,10)
	if(player.oxygen>20)then
		radius=player.oxygen*6/100
		circfill(player.x+10,player.y-10, player.oxygen*6/100,12)
		circ(player.x+10,player.y-10, player.oxygen*6/100,3)
	else
		circfill(player.x+10,player.y-10, player.oxygen*12/100+1,8) 
	end
	if(player.fuel>20)then
		circfill(player.x+15,player.y-5, player.fuel*6/100,10)
		circ(player.x+15,player.y-5, player.fuel*6/100,3)
	else
		circfill(player.x+15,player.y-5, player.fuel*12/100+1,8) 
	end
end


-->8
--effects
myEffects= {
	effects= {},
	fire={grav=true,grow=false,shrink=true},
	exposion={grav=true,grow=false,shrink=true},
	trail={grav=false,grow=false,shrink=true},

	AddRocket = function(self,o)
		add(effects,{x=o.x,y=o.y,t=o.t,die=o.die,dx=o.dx,dy=o.dy,grav=o.grav,grow=o.grow,shrink=o.shrink,r=o.r,c=o.c,c_table=o.c_table})
	end,
	AddTrail = function(self,o)
		for i=0,o.w  do
			--settings
			add(self.effects,{
					name="trail",
					x=o.x+4,
					y=o.y+4,
					w=2,
					h=2,
					time=0,
					die=30+rnd(10),
					dx=rnd(10)-5,
					dy=rnd(10)-5,
					character=self.trail,
					r=o.w/6,
					c=o.f.c,
					c_table=o.f.c_table,
				}
			)
		end

	end,
	AddExplode = function(self,o,num)
		for i=0, num do
			--settings
			add(self.effects,{
					name="explosion",
					x=o.x+4,
					y=o.y+4,
					w=2,
					h=2,
					time=0,
					die=rnd(10),
					dx=rnd(2)-1,
					dy=rnd(2)-1,
					character=self.exposion,
					r=4,
					c=10,
					c_table={10,9,8,7},
				}
			)
		end
	end,
	AddFire = function(self,o)
		if o.direction.left != "none" then
			if o.direction.left == "down" then
				_dy=o.f.dy
			elseif o.direction.left == "up" then
				_dy=-o.f.dy
			else
				_dy=0
			end

			for i=0,o.f.amount do
				add(self.effects,{
						name="fire",
						x=o.x-2,
						y=o.y+9,
						w=2,
						h=2,
						time=0,
						die=o.f.die+rnd(10),
						dx=-o.f.dx+rnd(1)-0.5,
						dy=_dy+rnd(1)-0.5,
						character=self.fire,
						r=o.f.r,
						c=o.f.c,
						c_table=o.f.c_table
					}
				)
			end
		end
		if o.direction.right != "none" then
			if o.direction.right == "down" then
				_dy=o.f.dy
			elseif o.direction.right == "up" then
				_dy=-o.f.dy
			else
				_dy=0
			end

			for i=0,o.f.amount do
				add(self.effects,{
						name="fire",
						x=o.x+9,
						y=o.y+9,
						w=1,
						h=1,
						time=0,
						die=o.f.die+rnd(10),
						dx=o.f.dx+rnd(1)-0.5,
						dy=_dy+rnd(1)-0.5,
						character=self.fire,
						r=o.f.r,
						c=o.f.c,
						c_table=o.f.c_table
					}
				)
			end
		end
	end,

	update = function(self)
		for fx in all(self.effects) do
			--lifetime
			fx.time+=1
			if fx.time>fx.die then del(self.effects,fx) end
							
			--color depends on lifetime
			if fx.time/fx.die < 1/#fx.c_table then fx.c=fx.c_table[1]
			elseif fx.time/fx.die < 2/#fx.c_table then fx.c=fx.c_table[2]
			elseif fx.time/fx.die < 3/#fx.c_table then fx.c=fx.c_table[3]
			else fx.c=fx.c_table[4] end

			--physics
			--if fx.character.grav then fx.dy+=.05 end
			if level == 0 then fx.dy+=.1
			elseif level == 2 then fx.dy-=.1
			end
			
			if fx.character.grow then fx.r+=.1 end
			if fx.character.shrink then fx.r-=.25 end
			
			--move
			fx.x+=fx.dx
			fx.y+=fx.dy

			--check collisions

			for e in all(enemy) do
				distance = abs(player.x-e.x)+abs(player.y-e.y)
				if(distance < 100 and fx.name == "fire")then
					if collision(e,fx) then
						myEffects:AddExplode(fx,2)	
						e.dx+=fx.dx/64
						e.dy+=fx.dy/64
						del(self.effects,fx)
						
						e.w-=0.5
						e.h-=0.5
					end
				end
			end
		end
	end,
	draw = function(self)
		for fx in all(self.effects) do
			--draw pixel for size 1, draw circle for larger
			if(fx.c > 0) then
				if fx.r<=1 then
					pset(fx.x,fx.y,fx.c)
				else
					circfill(fx.x,fx.y,fx.r,fx.c)
				end
			end
		end
	end,
}	


-->8
--debug
debug1 = ""
debug2 = ""
debug3 = ""
debug4 = ""

function debug_draw()
	if(debug)then
		print(debug1,2,8,1)
		print(debug2,2,14,1)
		print(debug3,2,20,1)
		print(debug4,2,26,1)
	end
end


__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000780087000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000007070070700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000006700007600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000060000000060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00066600000666000006660000000777076007770000077700000000006660000066600000666000000000000000000000000000000000000000000000000000
06602a600660a260066a2260000007078006670700000707000000000622a65006220650062a0650000000000000000000000000000000000000000000000000
56555566565555665655556600000677076007770000077700000000055555550555555505555555000000000000000000000000000000000000000000000000
55577706555777065557770600006000000000000000000000000000607775556077755560777555000000000000000000000000000000000000000000000000
55777706557777065577770600760000000000000000000000000000607777556077775560777755000000000000000000000000000000000000000000000000
55777006557770065577700607070000000000000000000000000000600777506007775060077750000000000000000000000000000000000000000000000000
07770060077700600777006008700000000000000000000000000000060077700600777006007770000000000000000000000000000000000000000000000000
00666600006666000066660000000000000000000000000000000000006666000066660000666600000000000000000000000000000000000000000000000000
22067066220670662206706677700000777006707770000000000000660760226607602266076022000000000000000000000000000000000000000000000000
26226666262266662622666670700000707660087070000000000000666622626666226266662262000000000000000000000000000000000000000000000000
26666666266666662666666677600000777006707770000000000000666666626666666266666662000000000000000000000000000000000000000000000000
2666aa6026666a60266a6a600006000000000000000000000000000006aa666206a6666206a6a662000000000000000000000000000000000000000000000000
2666aa602666a6602666a6600000670000000000000000000000000006aa6662066a6662066a6662000000000000000000000000000000000000000000000000
22666660226a66602266a66000007070000000000000000000000000066666220666a622066a6622000000000000000000000000000000000000000000000000
0266666002a66660026a6a60000007800000000000000000000000000666662006666a2006a6a620000000000000000000000000000000000000000000000000
02266600022666000226660000000000000000000000000000000000006662200066622000666220000000000000000000000000000000000000000000000000
00000000000770000000770000077000000770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000066665500066665505666660056666600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000066665500066665505566660055666600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000006005000006005000500600005006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000006005000006005000500600005006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000066005500066005505600660056006600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000088008800055005508800880055005500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000888088800555055508880888055505550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000011111110000000000000000001111110000000000110111000000000011110000000000001100010000000000000000000000000000000000000
00000000011222222211000000000000000001222221100000011110112110000001122211100000000112100111100000000000000000000000000000000000
00000001122288888821000000000000001101111182210000112211018221000012281111001100001221100122110000000000000000000000000000000000
00000012288888888821000001000000011211000111821000128811018882100128110000111110012881001122210000000000000000000000000000000000
00000122888888888822100112100000012888111001111000018881001882110111000111122210118881001228210000000000000000000000000000000000
00001228888888888882211448210000122888888111000011018881101882210000111122288211128810001288100100000000000000000000000000000000
00012888888888888888222488821000011222888882110011001888101288210011118888882211128210018882101100000000000000000000000000000000
00122888888888888888888888821000000112222882221111001882100228211111888882222110128210012881001100000000000000000000000000000000
00128888888888888888881111110000000000112228822111101882210122211188888811100000122200112821001100000000000000000000000000000000
01228888888888881111110000000000111000000122222112110188210012211888881100000011122100122810012100000000000000000000000000000000
01288888881111110000000000000110121111000001118112810188221012211881110000011121122100228810012100000000000000000000000000000000
01281111110000000000001111111111012821110000011101210118221001110110000111188211011001282100121000000000000000000000000000000000
01110000000000001111118888888821012888821110000001211018221101100000111888888210011001222100121000000000000000000000000000000000
00000000002222228888888888888821001288888821100000121012822100000011888888882100000012221001210000000000000000000000000000000000
00001111114888888888888888888821000112288881100000111001122100000001188888211000000012221001100000000000000000000000000000000000
02118221144888888888888884448821000001111111000000001100111100000000011111100000000001110000000000000000000000000000000000000000
12888210014888888888888822248821011110000011100000111000000111000000000000000000000000000000000000000000000000000000000000000000
12888210014888888888888211118821112210000112210001222210011211000000000000000000000000000000000000000000000000000000000000000000
12888221124888888888822210011111122210101121100101018810018810000000000000000000000000000000000000000000000000000000000000000000
12288882228888888888821100000000121101110110011112001181128100110000000000000000000000000000000000000000000000000000000000000000
01288888888888881111110000000000110012110100188112200011181001810000000000000000000000000000000000000000000000000000000000000000
01288888881111110000000000001110001182110001881101222010110018210000000000000000000000000000000000000000000000000000000000000000
01111111110000000000001111111110001421100011211001111100000182110000000000000000000000000000000000000000000000000000000000000000
00000000000000001111118888882100001110000001111000000000000111100000000000000000000000000000000000000000000000000000000000000000
00000000001111118888888888821100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000111118888888888888888821000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00001288888888888888888888210000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000122888888888888888822100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000012228888888888888281000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000001122222888888222110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000011222222222111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000011111111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001111100111111001111110000000000111111001111100001110000111110000bb0000000bb000000bb0000000000000000000000000000000000000000000
011bbb10111111111166aa1100000000116fff1111eee110011d11101122211000bbb000000bbb0000bbb0000000000000000000000000000000000000000000
01bbbb111c111cc1166aaaa10000000016fffff11eeeee1111dddd111222221000ebbb00000ebbb00bbbe0000000000000000000000000000000000000000000
11bbbbb11cc11cc1166aaaa1000000001ffffff11eeeeee11dddddd11222221100eeb000000eeb0000bee0000000000000000000000000000000000000000000
16bbbbb11cccccc116aaaaa1000000001fffff611eeeeee11dddddd11222222100dd0000000dd000000dd0000000000000000000000000000000000000000000
116bbb1111cccc1111aaaa110000000011ffff111eeeee111dddddd11222221100d00000000d00000000d0000000000000000000000000000000000000000000
0116bb10011cc110011aa110000000000116f11011eee11011dddd111122211000d00000000d00000000d0000000000000000000000000000000000000000000
001111100011110000111100000000000011110001111100011111100111110000d00000000d00000000d0000000000000000000000000000000000000000000
00111100011111000111110000000000011111000011110000111110000111000000000000000000000000000000000000000000000000000000000000000000
111bb11111ccc11011aaa1100000000011fff110011ee110011ddd11011121100000000000000000000000000000000000000000000000000000000000000000
1bbbbbb111cccc111aaaaa11000000001ffff61111eeee1101ddddd1112222110000000000000000000000000000000000000000000000000000000000000000
1bbbbbb11111ccc11aaaaaa1000000001ffffff11eeeeee111ddddd1122222210000000000000000000000000000000000000000000000000000000000000000
16bbbbb11111ccc116aaaaa1000000001ffffff11eeeeee11dddddd1122222210000000000000000000000000000000000000000000000000000000000000000
116bbb11111ccc111666aa11000000001ffff6111eeeeee111ddddd1122222210000000000000000000000000000000000000000000000000000000000000000
0111611011ccc110116661100000000011ff611011eeee11011ddd11112222110000000000000000000000000000000000000000000000000000000000000000
00011100011111000111110000000000011111000111111000111110011111100000000000000000000000000000000000000000000000000000000000000000
01111100001111000011110000000000001111000011111001111110001111100000000000000000000000000000000000000000000000000000000000000000
01bbb110011cc110011aa11000000000011ff110011eee1111dddd11011222110000000000000000000000000000000000000000000000000000000000000000
11bbbb1111cccc1111aaaa110000000011ffff1111eeeee11dddddd1112222210000000000000000000000000000000000000000000000000000000000000000
1bbbbbb11cccccc11aaaaa61000000001ffffff11eeeeee11dddddd1122222210000000000000000000000000000000000000000000000000000000000000000
16bbbb111cc11cc11aaaa661000000001ffffff11eeeeee11dddddd1112222210000000000000000000000000000000000000000000000000000000000000000
11bbbb101cc111c11aaaa6610000000016fffff111eeeee111dddd11012222210000000000000000000000000000000000000000000000000000000000000000
016b61101111111111aa661100000000116fff11011eee110111d110011222110000000000000000000000000000000000000000000000000000000000000000
01111100011111100111111000000000011111100011111000011100001111100000000000000000000000000000000000000000000000000000000000000000
00111000001111100011111000000000001111100111111001111100011111100000000000000000000000000000000000000000000000000000000000000000
011b1110011ccc110116661100000000011fff1111eeee1111ddd110112222110000000000000000000000000000000000000000000000000000000000000000
11bbbb1111ccc11111aa66610000000011fffff11eeeeee11ddddd11122222210000000000000000000000000000000000000000000000000000000000000000
1bbbbbb11ccc11111aaaaa61000000001ffffff11eeeeee11dddddd1122222210000000000000000000000000000000000000000000000000000000000000000
1bbbbbb11ccc11111aaaaaa1000000001ffffff11eeeeee11ddddd11122222210000000000000000000000000000000000000000000000000000000000000000
16bbbbb111cccc1111aaaaa10000000011fffff111eeee111ddddd10112222110000000000000000000000000000000000000000000000000000000000000000
1116b111011ccc11011aaa1100000000011fff11011ee11011ddd110011211100000000000000000000000000000000000000000000000000000000000000000
00111100001111100011111000000000001111100011110001111100001110000000000000000000000000000000000000000000000000000000000000000000
00000000000770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700000007007000000070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07077700070000700077707000777700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70000700007007000070000700700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70000700007007000070000700700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07077700007777000077707007000070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700000000000000000070000700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77777000000000000000000000077777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70070000000077000077000000007007000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70007000000700700700700000070007000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77000700707000700700070700700077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70700070770007000070007707000707000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00070070700070000007000707007000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00007700700700000000700700770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000777770000007777700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
0000000000000000000000010100010000000000000000000000000000000000c4c5c6c7c8c9cacbcccdcecf000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000c1000000000000000000000000000000000000000000000000d4d5d6d7d8d9dadbdcdddedf000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000d00000000000000000d3000000e4e5e6e7e8e9eaebecedeeef000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000f4f5f6f7f8f9fafbfcfdfeff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000c000000000000000000000c2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000d100000000000000d200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000c3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
01010000292702927029270162702927029270292702927029270292702a2702a2702a2702a2702a2702b2702b2702d270332703527034270322502a25022250162501525021250232502725028250292502b250
010100000061001610016100161001610016100161001610000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
01010000000001d7101e71020710227102f71006710077100671007710087100871006710067100a7100b7100b7100b7100b7100c7100d7100d7100e7100771011710117101471013710127001f7001071000010
010100000000000000000100871009710077100871008710087100871009710087100871000010000000d7100e7101171013710167101c710217102b710217101b71019710177101570013710117000000000000
0001000000000000000f6500d2500f25011250122501625029450192502c6501d25035650202502125023250242502525026250282502a2502c2502b4502a4502775027750287502875028750000000000000000
000100000000000000000000000000000000001605015050140501405014050000001405014050140500000014050140501405014050130500000000000000000000000000000000000000000000000000000000
00010000000000255002550035500455005550065500755008550085502d5502d5502c5502a550245501c55015550135500b5500b5500c5500e550135501e550155500e5500a5500855007550045500455000000
0001000001020030200702007020070200f0200603006030050300403002030080300003001030010300103001030010200101002050020500201003010010100001000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000200002f6602f6502f6503b650386503665034640306402d6402c64029640276302563022630206301f6201c6201962018620156201362012620106200f6200e6200d6100b6100a61008610066100361001600
0002000026650276513e15128151371511b1512e1512a15125151211511c15118151131510d1510a15108151051510215101151001510015111001100010e0010c0010b001080010700106001040010200100001
000100002665026650256502c5502a550275502555022550205501d5501b550185501555013550105500e5500b55008550065500355000550001500c1000b1000a10007100061000410002100011000010000200
000100002c05016070305702b50028500215001e5001a500135000f50008500045000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
01030000380723b0720000200002380623b0620000200002380523b0520000200002380423b0420000238002380323b0320000200002380223b0220000200002380123b0020000238002380023b0020000200002
000100000461006610076200a6300d64010650156601b670206702a670326703a6703c6602f65026650216401b630166301262009610046100061006600036000160000600006000000000000000000000000000
00030000076540b65414654176541b6541f65424654296542f65436654396543b6543c6543e6543e6543e6543e6543e6543e6543e6543e6543e6543e6443e6243a614356142e61429614226141b6141361409604
010800001434413344003000030000300003000030000300003000030000300003000030000300003000030000300003000030000300003000030000300003000030000300003000030000300003000030000300
000a00002d535245051b535155053373526735000051a73500505215550b5551a7053d05520705197551c5053a5353c505000051f75500005000052f7553670536755000053775521055000053a5552750520755
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011e00000042500425004250042500425004250042500425004250042500425004250042500425004250042500425004250042500425004250042500425004250042500425004250042500425004250042500425
011e00000c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c4170c417
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00100000130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044130440c0440f0440c044
00140000130240c0210f02400000130240c0240f0240c024130240c0210f02400000130240c0240f02400000130240c0210f02400000130240c0240f02400000130240c0210f02400000130240c0240f02400000
0010000000000000000f0140c014130140c0140f0140c01400000000000f0140c014130140c0140f0140c01400000000000f0140c014130140c0140f0140c01400000000000f0140c014130140c0140f0140c014
00100000130140c0140f0140c014130140c0140000000000130140c0140f0140c014130140c0140000000000130140c0140f0140c014130140c0140000000000130140c0140f0140c014130140c0140000000000
001000000c0371903700037050370c0471a0371b037050570e0370004700047010470c0470004700047111470c0470004700047000470c0470004700047111470e0570005700057000570c057000570000711157
001000000c0101900000000180000c0101a0001b0001b0000e0100000000000000000c0100000000000000000c0100000000000000000c0100000000000000000e0100000000000000000c010000000000000000
001000000c0101900000000180000c0101a0001b0001b0000e0100000000000000000c0100000000000000000c0100000000000000000c0100000000000000000e0100000000000000000c010000000000000000
001000000c0101900000000180000c0101a0001b0001b0000e0100000000000000000c0100000000000000000c0100000000000000000c0100000000000000000e0100000000000000000c010000000000000000
011400000c7150070500705007050c7150070500705007050c7150070500705007050c7150070500705007050c7150070500705007050c7150070500705007050c7150070500705007050c715007050070500705
001e00000c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c023000030000300003
001e00000c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c023000030000300003
001200000c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c0230000300003000030c023000030000300003
001000000e0400e0300e0100e0400e0300e0200e0100e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e020
001000000e0400e0300e0100e0400e0300e0200e0100e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e020
001000000e0400e0300e0100e0400e0300e0200e0100e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e020
001000000e0400e0300e0100e0400e0300e0200e0100e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e0200e020
011e0000007050c7250e72510725117250e725107250c72513725007051872500705177260070518725007051a725137251572517725187251572518725137251a725007051f725007051d726007051f72500705
011000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__music__
00 21614644
00 20614344
00 21424344
02 41214344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
01 6064286c
00 21422844
00 62422844
02 63422844

