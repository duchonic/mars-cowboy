# space cowboy


The blood tide of steel (metal) beats the pulse.。。。
In exchange for today's Sun coins aim at yesterday's friends! 
I forgot my name (code), but I remember the taste of victory (oil).
The sign of death resonates in the red soil where the plants also die!! 
The Battle of 32 people to wash the cooling water (Ch) with lubricating oil! 

second transaltion:

he blood of steel pulses through my veins. I'll take aim at yesterday's friends in exchange for the day's money! I forget my name (code), but I remember the taste of victory (oil). The signal to die is sounded in the red earth, where plants and trees are dying! A 32-player match to wash the coolant with oil begins

third translation (gold_ninja from itch.io):

yamamu | ゲームアーテイスト (Game artist)

Steel (metal) blood courses through your veins... Hunt down your former friends to make a buck just to survive. You forgot your name (code) but you remember the taste of victory (oil). Signs of death echo across the dusty, lifeless red earth!! It's lubricant (blood) for coolant (blood) as this 32 player battle erupts! MARS COWBOY!!!!!

---

<!-- fg=black bg=yellow -->
# MARS COWBOY!
![RC](data/223_sample.jpg)

---


## characters

* mechanic
* biologist
* coder

### first

machanical engineer is equipped with rockets

* power 75%
* oxygen 75%

![](data/marscowboy_000.png)

### second

the biologist has no equippement

* power 100%
* oxygen 100%

![](data/marscowboy_001.png)

### third

software engineer is euqipped with a shield

* power 50%
* oxygen 50%

![](data/marscowboy_002.png)
---

## story

### region 1 

Normal gravitiy downwords

![](data/normal.png)

### region 2

No gravity at all

![](data/space.png)

### region 3

Gravity inverted, goes up

![](data/hard.png)

---

## sound

### effects


### music



---


<!-- effect=explosions -->
---
some code
```lua
--this is some code in lua
function blub()
 print("blob",10,20)
end
```

# progress so far

## pretotype day 0

![](data/marscowboy_0.gif)

![](data/marscowboy_1.gif)

![](data/marscowboy_2.gif)

## pretotype day 1

menu, compass:

![](data/marscowboy_3.gif)

head- and bodycollisions:

![](data/marscowboy_4.gif)

## pretotype day 2

enemies

![](data/marscowboy_5.gif)

## pretotype day 8

oxygen and power

![](data/marscowboy_6.gif)

## prototype day 9
![](data/marscowboy_8.gif)

## prototype day 10
![](data/marscowboy_9.gif)
----
